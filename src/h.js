import vnode from './vnode';
import { is } from './utils';

/**
 * 将参数转换为虚拟节点
 *
 * @param {string} sel
 * @param {object} data
 * @param {vnode | vnode[] | string} c
 * @returns vnode
 */
function h(sel, data, c) {
  let text, children;

  if (c && is.string(c)) text = c;
  if (c && is.array(c)) {
    children = [];
    for (let i = 0; i < c.length; i++) {
      // 舍弃掉类型错误的项
      if (!is.vnode(c[i])) {
        console.warn('Children must be vnode');
        continue;
      }
      children.push(c[i]);
    }
  }
  if (c && is.vnode(c)) children = [c];

  return vnode(sel, data, children, text);
}

export default h;
