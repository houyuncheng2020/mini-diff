/**
 * 根据参数生成一个虚拟节点
 *
 * @param {string} sel
 * @param {object} data
 * @param {vnode[]} children
 * @param {string} text
 * @param {HTMLElement} elm
 * @return {*} 
 */
function vnode(sel, data, children, text, elm) {

  // 把key从data中提取出来，不然无法进行最小化diff
  if (data['key']) {
    const key = data['key'];
    delete data['key'];
    return { sel, data, children, text, elm, key };
  }

  return { sel, data, children, text, elm };
}

export default vnode;