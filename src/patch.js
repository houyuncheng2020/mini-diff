import { is } from './utils';
import vnode from './vnode';
import createElem from './createElem';
import patchNode from './patchNode';

function elm2Vnode(elm) {
  return vnode(elm.tagName.toLowerCase(), {}, [], undefined, elm);
}

export function isSameNode(oldVnode, newVnode) {
  // 当选择器相同且key相同时，我们认为是同一个节点
  return oldVnode.sel == newVnode.sel && oldVnode.key == newVnode.key;
}

function patch(oldVnode, newVnode) {
  // 如果不是虚拟节点，将其转换为虚拟节点
  if (!is.vnode(oldVnode)) {
    oldVnode = elm2Vnode(oldVnode);
  }

  if (isSameNode(oldVnode, newVnode)) {
    // 是同一个虚拟节点
    // 精细化比较
    patchNode(oldVnode, newVnode);
  } else {
    // 不是同一个虚拟节点
    // 删除所有老节点，创建新节点
    const parent = oldVnode.elm.parentNode;
    // 新元素插入到旧元素的前面，再删除旧元素，可以保持元素位置不变
    parent.insertBefore(createElem(newVnode), oldVnode.elm);
    parent.removeChild(oldVnode.elm);
  }

  return newVnode;
}

export default patch;
