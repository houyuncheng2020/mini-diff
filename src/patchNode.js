import createElem from './createElem';
import updateChildren from './updateChildren';

function patchNode(oldVnode, newVnode) {
  // 新旧虚拟dom指向同一个对象，完全一样，啥也不做，直接返回
  if (oldVnode === newVnode) return;

  // 比较文本内容
  if (newVnode.text && (!newVnode.children || newVnode.children.length === 0)) {
    // 新节点有text属性
    if (newVnode.text !== oldVnode.text)
      oldVnode.elm.innerText = newVnode.text;
  } else {
    // 新节点没有text属性
    if (oldVnode.children && oldVnode.children.length > 0) {
      // 新旧节点都有子节点，操作可能为增删改
      updateChildren(oldVnode.elm, oldVnode.children, newVnode.children);
    } else {
      // 旧节点没有子节点，新节点有子节点
      // 清空旧节点中的文字
      oldVnode.elm.innerText = '';
      // 将新节点真实dom创建出来，并追加上树
      for (let i = 0, len = newVnode.children.length; i < len; i++) {
        oldVnode.elm.appendChild(createElem(newVnode.children[i]));
      }
    }
  }
  newVnode.elm = oldVnode.elm;
}

export default patchNode;
