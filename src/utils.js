export function toString(arg) {
  return Object.prototype.toString.call(arg);
}

export function hasOwn(obj, prop) {
  return obj.hasOwnProperty(prop);
}

export const is = {
  array: arg => toString(arg) === '[object Array]',
  vnode: arg =>
    toString(arg) === '[object Object]'
    && hasOwn(arg, 'sel'),
  string: arg => typeof arg === 'string',
}
