/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./src/createElem.js":
/*!***************************!*\
  !*** ./src/createElem.js ***!
  \***************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/**\r\n * 由虚拟dom创建真实dom\r\n *\r\n * @param {vnode} vnode\r\n * @return {Element} \r\n */\r\nfunction createElem(vnode) {\r\n  const {sel, data = {}, children = [], text = undefined} = vnode;\r\n\r\n  const elm = document.createElement(sel);\r\n  // 保存真实dom指向到虚拟dom\r\n  vnode.elm = elm;\r\n\r\n  for (const attr in data) {\r\n    // 处理类名\r\n    if (attr === 'class') {\r\n      for (const cls in data[attr]) {\r\n        if (data[attr][cls]) {\r\n          elm.classList.add(cls);\r\n        }\r\n      }\r\n      continue;\r\n    }\r\n    // 处理样式\r\n    if (attr === 'style') {\r\n      for (const s in data[attr]) {\r\n        elm.style[s] = data[attr][s];\r\n      }\r\n      continue;\r\n    }\r\n    // 添加属性\r\n    elm.setAttribute(attr, data[attr]);\r\n  }\r\n\r\n  if (text) elm.innerText = text;\r\n\r\n  // 递归生成子元素并添加到父元素\r\n  for (let i = 0, len = children.length; i < len; i++) {\r\n    elm.appendChild(createElem(children[i]));\r\n  }\r\n  \r\n  return elm;\r\n}\r\n\r\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (createElem);\r\n\n\n//# sourceURL=webpack://mini-diff/./src/createElem.js?");

/***/ }),

/***/ "./src/h.js":
/*!******************!*\
  !*** ./src/h.js ***!
  \******************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var _vnode__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./vnode */ \"./src/vnode.js\");\n/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./utils */ \"./src/utils.js\");\n\r\n\r\n\r\n/**\r\n * 将参数转换为虚拟节点\r\n *\r\n * @param {string} sel\r\n * @param {object} data\r\n * @param {vnode | vnode[] | string} c\r\n * @returns vnode\r\n */\r\nfunction h(sel, data, c) {\r\n  let text, children;\r\n\r\n  if (c && _utils__WEBPACK_IMPORTED_MODULE_1__.is.string(c)) text = c;\r\n  if (c && _utils__WEBPACK_IMPORTED_MODULE_1__.is.array(c)) {\r\n    children = [];\r\n    for (let i = 0; i < c.length; i++) {\r\n      // 舍弃掉类型错误的项\r\n      if (!_utils__WEBPACK_IMPORTED_MODULE_1__.is.vnode(c[i])) {\r\n        console.warn('Children must be vnode');\r\n        continue;\r\n      }\r\n      children.push(c[i]);\r\n    }\r\n  }\r\n  if (c && _utils__WEBPACK_IMPORTED_MODULE_1__.is.vnode(c)) children = [c];\r\n\r\n  return (0,_vnode__WEBPACK_IMPORTED_MODULE_0__.default)(sel, data, children, text);\r\n}\r\n\r\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (h);\r\n\n\n//# sourceURL=webpack://mini-diff/./src/h.js?");

/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"h\": () => (/* reexport safe */ _h__WEBPACK_IMPORTED_MODULE_0__.default),\n/* harmony export */   \"patch\": () => (/* reexport safe */ _patch__WEBPACK_IMPORTED_MODULE_1__.default)\n/* harmony export */ });\n/* harmony import */ var _h__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./h */ \"./src/h.js\");\n/* harmony import */ var _patch__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./patch */ \"./src/patch.js\");\n\r\n\r\n\r\n\r\n\n\n//# sourceURL=webpack://mini-diff/./src/index.js?");

/***/ }),

/***/ "./src/patch.js":
/*!**********************!*\
  !*** ./src/patch.js ***!
  \**********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"isSameNode\": () => (/* binding */ isSameNode),\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./utils */ \"./src/utils.js\");\n/* harmony import */ var _vnode__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./vnode */ \"./src/vnode.js\");\n/* harmony import */ var _createElem__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./createElem */ \"./src/createElem.js\");\n/* harmony import */ var _patchNode__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./patchNode */ \"./src/patchNode.js\");\n\r\n\r\n\r\n\r\n\r\nfunction elm2Vnode(elm) {\r\n  return (0,_vnode__WEBPACK_IMPORTED_MODULE_1__.default)(elm.tagName.toLowerCase(), {}, [], undefined, elm);\r\n}\r\n\r\nfunction isSameNode(oldVnode, newVnode) {\r\n  // 当选择器相同且key相同时，我们认为是同一个节点\r\n  return oldVnode.sel == newVnode.sel && oldVnode.key == newVnode.key;\r\n}\r\n\r\nfunction patch(oldVnode, newVnode) {\r\n  // 如果不是虚拟节点，将其转换为虚拟节点\r\n  if (!_utils__WEBPACK_IMPORTED_MODULE_0__.is.vnode(oldVnode)) {\r\n    oldVnode = elm2Vnode(oldVnode);\r\n  }\r\n\r\n  if (isSameNode(oldVnode, newVnode)) {\r\n    // 是同一个虚拟节点\r\n    // 精细化比较\r\n    (0,_patchNode__WEBPACK_IMPORTED_MODULE_3__.default)(oldVnode, newVnode);\r\n  } else {\r\n    // 不是同一个虚拟节点\r\n    // 删除所有老节点，创建新节点\r\n    const parent = oldVnode.elm.parentNode;\r\n    // 新元素插入到旧元素的前面，再删除旧元素，可以保持元素位置不变\r\n    parent.insertBefore((0,_createElem__WEBPACK_IMPORTED_MODULE_2__.default)(newVnode), oldVnode.elm);\r\n    parent.removeChild(oldVnode.elm);\r\n  }\r\n\r\n  return newVnode; \r\n}\r\n\r\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (patch);\r\n\n\n//# sourceURL=webpack://mini-diff/./src/patch.js?");

/***/ }),

/***/ "./src/patchNode.js":
/*!**************************!*\
  !*** ./src/patchNode.js ***!
  \**************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var _createElem__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./createElem */ \"./src/createElem.js\");\n/* harmony import */ var _updateChildren__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./updateChildren */ \"./src/updateChildren.js\");\n\r\n\r\n\r\nfunction patchNode(oldVnode, newVnode) {\r\n  // 新旧虚拟dom指向同一个对象，完全一样，啥也不做，直接返回\r\n  if (oldVnode === newVnode) return;\r\n\r\n  // 比较文本内容\r\n  if (newVnode.text && (!newVnode.children || newVnode.children.length === 0)) {\r\n    // 新节点有text属性\r\n    if(newVnode.text !== oldVnode.text) \r\n      oldVnode.elm.innerText = newVnode.text; \r\n  } else {\r\n    // 新节点没有text属性\r\n    if (oldVnode.children && oldVnode.children.length > 0) {\r\n      // 新旧节点都有子节点，操作可能为增删改\r\n      (0,_updateChildren__WEBPACK_IMPORTED_MODULE_1__.default)(oldVnode.elm, oldVnode.children, newVnode.children);\r\n    } else {\r\n      // 旧节点没有子节点，新节点有子节点\r\n      // 清空旧节点中的文字\r\n      oldVnode.elm.innerText = '';\r\n      // 将新节点真实dom创建出来，并追加上树\r\n      for (let i = 0, len = newVnode.children.length; i < len; i++) {\r\n        oldVnode.elm.appendChild((0,_createElem__WEBPACK_IMPORTED_MODULE_0__.default)(newVnode.children[i]));\r\n      }\r\n    }\r\n  }\r\n  newVnode.elm = oldVnode.elm;\r\n}\r\n\r\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (patchNode);\r\n\n\n//# sourceURL=webpack://mini-diff/./src/patchNode.js?");

/***/ }),

/***/ "./src/updateChildren.js":
/*!*******************************!*\
  !*** ./src/updateChildren.js ***!
  \*******************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var _patch__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./patch */ \"./src/patch.js\");\n/* harmony import */ var _patchNode__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./patchNode */ \"./src/patchNode.js\");\n/* harmony import */ var _createElem__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./createElem */ \"./src/createElem.js\");\n\r\n\r\n\r\n\r\n/**\r\n * 当新旧节点都有子节点时进行的精细化比较，\r\n * 使用两对双指针进行比较\r\n * 1.新前-旧前\r\n * 2.新后-旧后\r\n * 3.新后-旧前\r\n * 4.新前-旧后\r\n *\r\n * @param {Element} parentElm\r\n * @param {vnode[]} oldCh 旧节点的子节点\r\n * @param {vnode[]} newCh 新节点的子节点\r\n */\r\nfunction updateChildren(parentElm, oldCh, newCh) {\r\n  // 旧前\r\n  let oldStartIdx = 0;\r\n  // 旧后\r\n  let oldEndIdx = oldCh.length - 1;\r\n  // 新前\r\n  let newStartIdx = 0;\r\n  // 新后\r\n  let newEndIdx = newCh.length - 1;\r\n  // 旧前节点\r\n  let oldStartVnode = oldCh[oldStartIdx];\r\n  // 旧后节点\r\n  let oldEndVnode = oldCh[oldEndIdx];\r\n  // 新前节点\r\n  let newStartVnode = newCh[newStartIdx];\r\n  // 新后节点\r\n  let newEndVnode = newCh[newEndIdx];\r\n\r\n  let keyMap = null;\r\n\r\n  while (oldStartIdx <= oldEndIdx && newStartIdx <= newEndIdx) {\r\n\r\n    // 跳过已经加上undefuned标记的项\r\n    if (!oldStartVnode) {\r\n      oldStartVnode = oldCh[++oldStartIdx];\r\n    } else if (!oldEndVnode) {\r\n      oldEndVnode = oldCh[--oldEndIdx];\r\n    } else if (!newStartVnode) {\r\n      newStartVnode = newCh[++newStartIdx];\r\n    } else if (!newEndVnode) {\r\n      newEndVnode = newCh[--newEndIdx];\r\n    }\r\n\r\n    // 1.新前-旧前  比较  如果是同一个节点，交由patchNode处理\r\n    if ((0,_patch__WEBPACK_IMPORTED_MODULE_0__.isSameNode)(oldStartVnode, newStartVnode)) {\r\n      (0,_patchNode__WEBPACK_IMPORTED_MODULE_1__.default)(oldStartVnode, newStartVnode);\r\n      // 移动指针位置\r\n      oldStartVnode = oldCh[++oldStartIdx];\r\n      newStartVnode = newCh[++newStartIdx];\r\n    } else if ((0,_patch__WEBPACK_IMPORTED_MODULE_0__.isSameNode)(oldEndVnode, newEndVnode)) {\r\n      // 2.新后-旧后\r\n      (0,_patchNode__WEBPACK_IMPORTED_MODULE_1__.default)(oldEndVnode, newEndVnode);\r\n      // 移动指针位置\r\n      oldEndVnode = oldCh[--oldEndIdx];\r\n      newEndVnode = newCh[--newEndIdx];\r\n    } else if ((0,_patch__WEBPACK_IMPORTED_MODULE_0__.isSameNode)(oldStartVnode, newEndVnode)) {\r\n      // 3.新后-旧前\r\n      (0,_patchNode__WEBPACK_IMPORTED_MODULE_1__.default)(oldStartVnode, newEndVnode);\r\n      // 移动新前指向的这个节点到老节点的旧后的后面\r\n      // 只要插入一个已经在dom树上的节点，这个节点就会被移动到插入到位置\r\n      parentElm.insertBefore(oldStartVnode.elm, oldEndVnode.elm.nextSibling);\r\n      // 移动指针位置\r\n      oldStartVnode = oldCh[++oldStartIdx];\r\n      newEndVnode = newCh[--newEndIdx];\r\n    } else if ((0,_patch__WEBPACK_IMPORTED_MODULE_0__.isSameNode)(oldEndVnode, newStartVnode)) {\r\n      // 4.新前-旧后\r\n      (0,_patchNode__WEBPACK_IMPORTED_MODULE_1__.default)(oldEndVnode, newStartVnode);\r\n      // 移动新前指向的这个节点到老节点的旧前的前面\r\n      parentElm.insertBefore(oldEndVnode.elm, oldStartVnode.elm);\r\n      // 移动指针位置\r\n      oldEndVnode = oldCh[--oldEndIdx];\r\n      newStartVnode = newCh[++newStartIdx];\r\n    } else {\r\n      // 都没有匹配到\r\n      // 缓存key与索引值的映射\r\n      if (!keyMap) {\r\n        keyMap = {};\r\n        for (let i = oldStartIdx; i < oldEndIdx; i++) {\r\n          const key = oldCh[i].key;\r\n          if (key !== undefined) {\r\n            keyMap[key] = i;\r\n          }\r\n        }\r\n      }\r\n      // 寻找当前这项(newStartIdx)这项在keyMap中的位置序号\r\n      const idxInOld = keyMap[newStartVnode.key];\r\n\r\n      if (idxInOld === undefined) {\r\n        // 没有在缓存中找到内容，说明当前项是全新的项，直接新增\r\n        parentElm.insertBefore((0,_createElem__WEBPACK_IMPORTED_MODULE_2__.default)(newStartVnode), oldStartVnode.elm);\r\n      } else {\r\n        // 不是全新的项，需要移动\r\n        const elmToMove = oldCh[idxInOld];\r\n\r\n        if (elmToMove) {\r\n          (0,_patchNode__WEBPACK_IMPORTED_MODULE_1__.default)(elmToMove, newStartVnode);\r\n          // 标记，表示这一项已经处理过了\r\n          oldCh[idxInOld] = undefined;\r\n          parentElm.insertBefore(elmToMove.elm, oldStartVnode.elm);\r\n        }\r\n      }\r\n      // 指针下移，只移动新前\r\n      newStartVnode = newCh[++newStartIdx];\r\n    }\r\n  }\r\n\r\n  // 继续判断是否有剩余节点\r\n  if (oldStartIdx <= oldEndIdx) {\r\n    // 旧节点有剩余节点，表示新节点删除了某些节点\r\n    for (let i = oldStartIdx; i <= oldEndIdx; i++) {\r\n      if (oldCh[i]) {\r\n        parentElm.removeChild(oldCh[i].elm);\r\n      }\r\n    }\r\n  } else if (newStartIdx <= newEndIdx) {\r\n    // 新节点有剩余节点，表示新节点新增了某些节点\r\n    for (let i = newStartIdx; i <= newEndIdx; i++) {\r\n      parentElm.insertBefore((0,_createElem__WEBPACK_IMPORTED_MODULE_2__.default)(newCh[i]), oldCh[oldStartIdx].elm);\r\n    }\r\n  }\r\n}\r\n\r\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (updateChildren);\r\n\n\n//# sourceURL=webpack://mini-diff/./src/updateChildren.js?");

/***/ }),

/***/ "./src/utils.js":
/*!**********************!*\
  !*** ./src/utils.js ***!
  \**********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"toString\": () => (/* binding */ toString),\n/* harmony export */   \"hasOwn\": () => (/* binding */ hasOwn),\n/* harmony export */   \"is\": () => (/* binding */ is)\n/* harmony export */ });\nfunction toString(arg) {\n  return Object.prototype.toString.call(arg);\n}\n\nfunction hasOwn(obj, prop) {\n  return obj.hasOwnProperty(prop);\n}\n\nconst is = {\n  array: arg => toString(arg) === '[object Array]',\n  vnode: arg => \n    toString(arg) === '[object Object]' \n    && hasOwn(arg, 'sel'),\n  string: arg => typeof arg === 'string', \n}\n\n\n//# sourceURL=webpack://mini-diff/./src/utils.js?");

/***/ }),

/***/ "./src/vnode.js":
/*!**********************!*\
  !*** ./src/vnode.js ***!
  \**********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/**\r\n * 根据参数生成一个虚拟节点\r\n *\r\n * @param {string} sel\r\n * @param {object} data\r\n * @param {vnode[]} children\r\n * @param {string} text\r\n * @param {HTMLElement} elm\r\n * @return {*} \r\n */\r\nfunction vnode(sel, data, children, text, elm) {\r\n  \r\n  // 把key从data中提取出来，不然无法进行最小化diff\r\n  if (data['key']) {\r\n    const key = data['key'];\r\n    delete data['key'];\r\n    return {sel, data, children, text, elm, key}; \r\n  }\r\n\r\n  return {sel, data, children, text, elm};\r\n}\r\n\r\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (vnode);\n\n//# sourceURL=webpack://mini-diff/./src/vnode.js?");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval devtool is used.
/******/ 	var __webpack_exports__ = __webpack_require__("./src/index.js");
/******/ 	
/******/ })()
;