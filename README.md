## 介绍

snabbdom的简单实现，包括创建虚拟DOM和patch函数实现最小化更新

## 使用

```javascript
import { h, patch } from './mini-diff.js';

const btn = document.getElementById('btn');

const vnode1 = h('ul', {}, [
  h('li', { key: 'A' }, 'A'),
  h('li', { key: 'B' }, 'B'),
  h('li', { key: 'C' }, 'C'),
])

const vnode2 = h('ul', {}, [
  h('li', { key: 'A' }, 'A'),
  h('li', { key: 'D' }, 'D'),
  h('li', { key: 'E' }, 'E'),
  h('li', { key: 'B' }, 'B'),
])

patch(document.getElementById('container'), vnode1);

btn.onclick = function () {
  patch(vnode1, vnode2);
}
```